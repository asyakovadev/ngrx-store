import * as fromPizzas from '../actions/pizza.action';

import { Pizza } from "../../models/pizza.model";

export interface PizzaState {
	entities: { [id: number] : Pizza},
	loaded: boolean,
	loading: boolean
}

export const initialState: PizzaState = {
	entities: {},
	loaded: false,
	loading: false
};

export function reducer(
	state = initialState,
	action: fromPizzas.PizzasAction
): PizzaState {

	switch(action.type) {
		case fromPizzas.LOAD_PIZZAS : {
			console.log('LOAD_PIZZAS re');
			return {
				...state,
				loading: true
			};
		}
		case fromPizzas.LOAD_PIZZAS_FAIL : {
			return {
				...state,
				loaded: false,
				loading: false,
			};
		}
		case fromPizzas.LOAD_PIZZAS_SUCCESS : {
			console.log('LOAD_PIZZAS_SUCCESS re');
			//reformat [] into [{id:1}, {id: 2}]
			const pizzas = action.payload;
			const entities = pizzas.reduce(
				(entities: {[id: number]: Pizza }, pizza: Pizza) => {
					return {
						...entities,
						[ pizza.id ]: pizza
					};
				},
				{
					...state.entities
				}
			);

			return {
				...state,
				loaded: true,
				loading: false,
				entities
			};
		}

		case fromPizzas.UPDATE_PIZZA_SUCCESS :
		case fromPizzas.CREATE_PIZZA_SUCCESS : {
			console.log('here');
			const pizza = action.payload;
			const entities = {
				...state.entities,
				[pizza.id]: pizza
			};
			return {
				...state,
				entities
			}
		}
		case fromPizzas.REMOVE_PIZZA_SUCCESS: {
			const pizza = action.payload;
			const { [pizza.id]: removed, ...entities } = state.entities;

			return {
				...state,
				entities
			}
		}
	}
	return state
}


export const getPizzasLoading = (state: PizzaState) => state.loading;
export const getPizzasLoaded = (state: PizzaState) => state.loaded;
export const getPizzasEntities = (state: PizzaState) => state.entities;
