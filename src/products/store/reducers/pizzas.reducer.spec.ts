import * as fromPizza from './pizza.reducer';
import * as fromActions from '../actions/pizza.action';
import { Pizza } from "../../models/pizza.model";

describe('PizzaReducer', ()=> {
	describe('undefined action', () => {
		it('should return the default state', () => {
			const { initialState } = fromPizza;
			const action = {} as any;
			const state = fromPizza.reducer(undefined, action);

			expect(state).toBe(initialState);
		})
	});
	describe('LOAD_PIZZA action', () => {
		it('should set loading to true', () => {
			const { initialState } = fromPizza;
			const action = new fromActions.LoadPizzas();
			const state = fromPizza.reducer(initialState, action);

			expect(state.entities).toEqual({});
			expect(state.loaded).toEqual(false);
			expect(state.loading).toEqual(true);
		})
	});

	describe('LOAD_PIZZA_SUCCESS action', () => {
		it('should map an array to entities', () => {
			const pizzas: Pizza[] = [
				{id:1, name: 'pizza1', toppings: []},
				{id:2, name: 'pizza2', toppings: []},
			];
			const entities = {
				1: pizzas[0],
				2: pizzas[1]
			};

			const { initialState } = fromPizza;
			const action = new fromActions.LoadPizzasSuccess(pizzas);
			const state = fromPizza.reducer(initialState, action);

			expect(state.entities).toEqual(entities);
			expect(state.loaded).toEqual(true);
			expect(state.loading).toEqual(false);
		})
	});
});
describe('PizzasReducer Selectors', () => {
	describe('getPizzaEntities', () => {
		it('should return .entities', () => {
			const entities: {[key: number]: Pizza} = {
				1: {id:1, name: 'pizza1', toppings: []},
				2: {id:2, name: 'pizza2', toppings: []},
			};

			const { initialState } = fromPizza;
			const previousState = {...initialState, entities};
			const slice = fromPizza.getPizzasEntities(previousState);

			expect(slice).toEqual(entities);

		})
	})
})
