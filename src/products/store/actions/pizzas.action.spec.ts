import * as fromPizza from './pizza.action';

describe('Pizzas Action', () => {
	describe ( 'LoadPizzas Actions', () => {
		describe ( 'Load Pizzas', () => {
			it ( 'should create an action', () => {
				const action = new fromPizza.LoadPizzas ();

				expect ( { ...action } ).toEqual ( {
					type: fromPizza.LOAD_PIZZAS,
				} )
			} )
		} );
		describe ( 'Load Pizzas Fail', () => {
			it ( 'should fail create an action', () => {
				const payload ={message: 'Load Error'};
				const action = new fromPizza.LoadPizzasFail (payload);

				expect ( { ...action } ).toEqual ( {
					type: fromPizza.LOAD_PIZZAS_FAIL,
					payload
				} )
			} )
		} );
		describe ( 'Load Pizzas Success', () => {
			it ( 'should success create an action', () => {
				const payload =[{
					"name": "Blazin' Inferno",
					"toppings": [
						{
							"id": 10,
							"name": "pepperoni"
						},
						{
							"id": 9,
							"name": "pepper"
						},
						{
							"id": 3,
							"name": "basil"
						},
						{
							"id": 4,
							"name": "chili"
						},
						{
							"id": 7,
							"name": "olive"
						},
						{
							"id": 2,
							"name": "bacon"
						}
					],
					"id": 1
				},
					{
						"name": "Seaside Surfin'",
						"toppings": [
							{
								"id": 6,
								"name": "mushroom"
							},
							{
								"id": 7,
								"name": "olive"
							},
							{
								"id": 2,
								"name": "bacon"
							},
							{
								"id": 3,
								"name": "basil"
							},
							{
								"id": 1,
								"name": "anchovy"
							},
							{
								"id": 8,
								"name": "onion"
							},
							{
								"id": 11,
								"name": "sweetcorn"
							},
							{
								"id": 9,
								"name": "pepper"
							},
							{
								"id": 5,
								"name": "mozzarella"
							}
						],
						"id": 2
					}];
				const action = new fromPizza.LoadPizzasSuccess (payload);

				expect ( { ...action } ).toEqual ( {
					type: fromPizza.LOAD_PIZZAS_SUCCESS,
					payload
				} )
			} )
		} );
	} )
});
