import * as fromToppings from './toppings.actions';

describe('Toppings Action', () => {
	describe ( 'LoadToppings Actions', () => {
		describe ( 'Load Toppings', () => {
			it ( 'should create an action', () => {
				const action = new fromToppings.LoadToppings ();

				expect ( { ...action } ).toEqual ( {
					type: fromToppings.LOAD_TOPPINGS,
				} )
			} )
		} );
	} );
	describe ( 'VisualiseToppings Actions', () => {
		describe ( 'Visualise Toppings', () => {
			it ( 'should create an action', () => {
				const action = new fromToppings.VisualizeToppings([1,2,3]);

				expect ( { ...action } ).toEqual ( {
					type: fromToppings.VISUALIZE_TOPPINGS,
					payload: [1,2,3],
				} )
			} )
		} );
	} )
})
