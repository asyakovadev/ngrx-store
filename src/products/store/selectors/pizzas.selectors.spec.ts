import {TestBed} from '@angular/core/testing';
import {StoreModule, Store, combineReducers} from "@ngrx/store";

import * as fromRoot from '../../../app/store/reducers';
import * as fromReducers from '../reducers';
import * as fromActions from '../actions';
import * as fromSelectors from '../selectors';

import {Topping} from "../../models/topping.model";
import {Pizza} from "../../models/pizza.model";


describe('Pizza Selectors', ()=> {
	let store: Store<fromReducers.ProductState>;

	const pizza1: Pizza = {
		id: 1,
		name: 'Beach',
		toppings: [
			{id: 1, name: 'bacon Kevin'},
			{id: 2, name: 'pepperoni Kevin'},
			{id: 3, name: 'tomato Kevin'},
		]
	};

	const pizza2: Pizza = {
		id: 2,
		name:'Aloha',
		toppings: [
			{ id: 1, name: 'bacon Kevin'},
			{ id: 2, name: 'pepperoni Kevin'},
			{ id: 3, name: 'tomato Kevin'},
		]
	};
	const pizza3: Pizza = {
		id: 3,
		name:'Hawaii',
		toppings: [
			{ id: 1, name: 'bacon Kevin'},
			{ id: 2, name: 'pepperoni Kevin'},
			{ id: 3, name: 'tomato Kevin'},
		]
	};

	const pizzas: Pizza[] = [pizza1, pizza2, pizza3];


	const entities = {
		1: pizzas[0],
		2: pizzas[1],
		3: pizzas[2],
	};

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				StoreModule.forRoot({
					...fromRoot.reducers,
					products: combineReducers(fromReducers.reducers)
				})
			]
		});

		store = TestBed.get(Store);

		spyOn(store, 'dispatch').and.callThrough();
	});

	describe('getPizzaState', () => {
		it('should return state of pizza store slice', () => {
			let result;

			store
				.select(fromSelectors.getPizzaState)
				.subscribe(value => (result = value));

			expect(result).toEqual({
				entities: {},
				loaded: false,
				loading: false,
			});

			store.dispatch(new fromActions.LoadPizzasSuccess(pizzas));

			expect(result).toEqual({
				entities,
				loaded: true,
				loading: false
			});
		});
	});

	describe('getPizzaEntities', () => {
		it('should return pizzas as entities', () => {
			let result;

			store
				.select(fromSelectors.getPizzasEntities)
				.subscribe(value => (result = value));

			expect(result).toEqual({});

			store.dispatch(new fromActions.LoadPizzasSuccess(pizzas));

			expect(result).toEqual(entities);
		});
	});
	describe("getSelectedPizza", () => {
		it("should return selected pizza as entity", () => {
			let result;
			let params;

			store.dispatch(new fromActions.LoadPizzasSuccess(pizzas));
			store.dispatch({
				type: "ROUTER_NAVIGATION",
				payload: {
					routerState: {
						url: "/products",
						queryParams: {},
						params: { pizzaId: "2" }
					},
					event: {}
				}
			});
			store
				.select(fromRoot.getRouterState)
				.subscribe(routerState => (params = routerState.state.params));

			expect(params).toEqual({ pizzaId: "2" });

			store
				.select(fromSelectors.getSelectedPizza)
				.subscribe(selectedPizza => (result = selectedPizza));

			expect(result).toEqual(entities[2]);
		});
	});
	describe("getPizzaVisualised", () => {
		it("should return selected pizza composed with selected toppings", () => {
			let result;
			let params;

			const toppings = [
				{
					id: 6,
					name: 'mushroom'
				},
				{
					id: 9,
					name: 'pepper'
				},
				{
					id: 11,
					name: 'sweetcorn'
				}
			];

			store.dispatch(new fromActions.LoadPizzasSuccess(pizzas));
			store.dispatch(new fromActions.LoadToppingsSuccess(toppings));
			store.dispatch(new fromActions.VisualizeToppings([11, 9, 6]));

			store.dispatch({
				type: "ROUTER_NAVIGATION",
				payload: {
					routerState: {
						url: "/products",
						queryParams: {},
						params: { pizzaId: "2" }
					},
					event: {}
				}
			});
			store
				.select(fromSelectors.getPizzaVisualized)
				.subscribe(selectedPizza => (result = selectedPizza));

			const expectedToppings = [toppings[2], toppings[1], toppings[0]];

			expect(result).toEqual({...entities[2], toppings: expectedToppings});

		});
	});
});
